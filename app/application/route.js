import Ember from 'ember';

const redirectRoutes = [
  'index'
];

export default Ember.Route.extend({
  redirect(model, transition){
    if (redirectRoutes.includes(transition.targetName)) {
      return this.replaceWith('settings');
    }
  }
});
