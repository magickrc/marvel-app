import DS from 'ember-data';

const {attr} = DS;

export default DS.Model.extend({
  firstName: attr('string'),
  lastName: attr('string'),
  email: attr('string'),
  executive: attr('boolean'),
  crm: attr('boolean'),
  marketing: attr('boolean'),
  compliance: attr('boolean'),
  risk: attr('boolean'),
  reporting: attr('boolean')
});