import Ember from 'ember';

const {set} = Ember;

export default Ember.Route.extend({
  setupController(controller, model) {
    this._super(...arguments);

    set(controller, 'title', 'New User');
  }
});
