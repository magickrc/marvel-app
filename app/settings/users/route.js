import Ember from 'ember';
const {get, set} = Ember;

export default Ember.Route.extend({

  actions: {
    createUser(userInfo) {
      let store = get(this, 'store');
      let user = store.createRecord('user', userInfo);

      user.save().then(() => {
        this.send('showModal', 'invite', get(user, 'email'));
      })
    },

    updateUser(user) {
      let store = get(this, 'store');
      // let user = store.createRecord('user', userInfo);

      user.save().then(() => {
        // set(this, 'controller.showModal', true);
        this.transitionTo('settings.users');
      })
    },

    deleteUser(user) {
      user.destroyRecord();
    },

    resetPassword(user) {
      this.send('showModal', 'reset', get(user, 'email'));
    },

    showModal(type, email) {
      let title = '', msg = '';

      if (type === 'invite') {
        title = 'Invite Sent';
        msg = `An invitation has been sent to email adress ${email}`
      } else if(type === 'reset') {
        title = 'Conformation Sent';
        msg = `A password conformation reset has been sent to email adress ${email}`
      }

      set(this, 'controller.modalType', type);
      set(this, 'controller.modalTitle', title);
      set(this, 'controller.modalMsg', msg);
      set(this, 'controller.showModal', true);
    },

    closeModal(type) {

      set(this, 'controller.modalType', null);
      set(this, 'controller.modalTitle', null);
      set(this, 'controller.modalMsg', null);
      set(this, 'controller.showModal', false);

      if (type === 'invite') {

      } else if(type === 'reset') {
        this.transitionTo('settings.users');
      }
    }
  }
});