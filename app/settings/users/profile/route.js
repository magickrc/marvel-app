import Ember from 'ember';

const {get, set} = Ember;

export default Ember.Route.extend({
  model(param) {
    return this.store.findRecord('user', param.user_id);
  },

  setupController(controller, model) {
    this._super(...arguments);

    set(controller, 'title', get(model, 'firstName'));
  }
});
