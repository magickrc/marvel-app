import Ember from 'ember';

const {set} = Ember;

export default Ember.Route.extend({
  model() {
    return this.store.findAll('user');
  },

  setupController(controller, model) {
    this._super(...arguments);

    set(controller, 'title', 'Users');
  }
});
