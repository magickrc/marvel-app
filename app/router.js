import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('settings', function() {
    this.route('users', function() {
      this.route('profile', { path: '/:user_id' });
      this.route('new');
    });
  });
});

export default Router;
