export default function(){
  this.transition(
    this.hasClass('popup'),
    this.toValue(true),
    this.use('crossFade', {duration: 200}),
    this.reverse('crossFade', {duration: 400})
  );

  this.transition(
    this.fromRoute('settings.index'),
    this.toRoute('settings.users'),
    this.use('toLeft', {duration: 200}),
    this.reverse('toRight', {duration: 200})
  );

  this.transition(
    this.fromRoute('settings.users.index'),
    this.toRoute('settings.users.profile'),
    this.use('toLeft', {duration: 200}),
    this.reverse('toRight', {duration: 200})
  );

  this.transition(
    this.fromRoute('settings.users.index'),
    this.toRoute('settings.users.new'),
    this.use('crossFade', {duration: 0}),
    this.reverse('toRight', {duration: 200})
  );

};