import Ember from 'ember';
const {Component, computed} = Ember;
const {readOnly} = computed;


export default Component.extend({
  visibleBackLink: readOnly('backRoute')
});
