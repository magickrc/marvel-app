import Ember from 'ember';

const {get, getProperties, setProperties, assign, $} = Ember;

export default Ember.Component.extend({
  classNames: ['user-edit-form'],

  fields: [
    'firstName',
    'lastName',
    'email',
    'executive',
    'crm',
    'marketing',
    'compliance',
    'risk',
    'reporting'
  ],

  init() {
    this._super(...arguments);

    let user = get(this, 'user');
    if (user) {
      setProperties(this, user.toJSON());
    }
  },

  actions: {
    create() {
      let fields = get(this, 'fields');
      let data = getProperties(this, fields);

      if (!get(data, 'firstName') || !get(data, 'lastName') || !get(data, 'email')) {
        $('.page-content').animate({ scrollTop: 0 }, "fast");
        return;
      }

      get(this, 'submit')(data);
    },

    update() {
      let fields = get(this, 'fields');
      let user = get(this, 'user');
      let data = getProperties(this, fields);

      assign(user, data);

      get(this, 'updateUser')(user);
    },

    resetPswd() {
      let user = get(this, 'user');

      get(this, 'resetPassword')(user);
    }
  }
});
