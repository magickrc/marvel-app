import { Factory } from 'ember-cli-mirage';

export default Factory.extend({
  firstName(i) { return `Person ${i}`; },
  lastName(i) { return `LastName ${i}`; },
  email(i) { return `email${i}@email.com`; },
  executive: false,
  crm: false,
  marketing: false,
  compliance: false,
  risk: false,
  reporting: true
});
